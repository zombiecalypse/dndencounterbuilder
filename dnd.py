import collections
import fractions
import pprint
import random
import functools
import argparse

import dnd_stats

parser = argparse.ArgumentParser(description='Generate DnD encounters.')
parser.add_argument('--level', '-l', action='append', type=int, default=[])
parser.add_argument('--num', '-n', type=int, default=1)
parser.add_argument('--monsters', '-m', type=int, default=None)
parser.add_argument('--difficulty', '-d', type=str, default='medium')
parser.add_argument('--encounters', '-e', type=int, default=1)

FORMAT_LINE = """
Proficiency: {prof:d}   AC: {ac:d}  HP: {hp:d}  Save: {save_dc:d}
Attack: {attk:d} ({dmg}) based on {attk_with}
Attributes: {attr[str]:+3d} | {attr[dex]:+3d} | {attr[con]:+3d}
            {attr[int]:+3d} | {attr[wis]:+3d} | {attr[cha]:+3d}
Attacks:
    {attks}
Special:
    {effects}
""".strip()

def FormatAttk(bonus, dc, attk):
    lines = []
    if attk.save_based:
        lines.append('%3d DC%d' % (attk.dmg, dc))
    else:
        lines.append('%+3d (%3d)' % (bonus, attk.dmg))
    if attk.area is not None:
        lines.append('    Area: %s %s' % (attk.area.range, attk.area.shape))
    if attk.multi_attk != 1:
        lines.append('    Multiattack: %s' % attk.multi_attk)
    for e in attk.effects:
        lines.append('    %s' % e)
    return lines

if __name__ == '__main__':
    args = parser.parse_args()
    if args.difficulty not in ['easy', 'medium', 'hard', 'deadly']:
        raise ValueError('not a valid difficulty: %r' % args.difficulty)
    for i in range(args.encounters):
        if args.encounters > 1:
            print('=== Encounter #{} ==='.format(i+1))
        enc = dnd_stats.ScoredEncounter(10, args.level * args.num, difficulty=args.difficulty,
                n_monsters=args.monsters)

        for n, monster in enc:
            if n == 1:
                print(' 1 Monster:')
            else:
                print('{n:2d} Monsters:'.format(n=n))
            print('\n'.join(['    '+l for l in FORMAT_LINE.format(
                    prof=monster.template.prof,
                    ac=monster.template.ac,
                    hp=monster.template.hp,
                    save_dc=monster.template.save_dc,
                    attk=monster.template.attk,
                    dmg=monster.template.dmg,
                    effects='\n    '.join(monster.effects),
                    attk_with=monster.attk_attr,
                    attr=monster.attr,
                    attks='\n    '.join(sum([FormatAttk(monster.template.attk,
                            monster.template.save_dc, a)
                            for a in  monster.attks], [])),
                    ).splitlines()]))
