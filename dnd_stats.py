import collections
import fractions
import functools
import itertools
import math
import pprint
import random
import cProfile

Props = [
        'prof',
        'ac',
        'hp', 
        'attk', 
        'dmg',
        'save_dc',
]

MB = collections.namedtuple('MB', Props)
Monster = collections.namedtuple('Monster', [
        'template',
        'effects',
        'cr',
        'attk_attr',
        'attr',
        'attks',
])

class Interval:
    def __init__(self, x, y=None):
        self.x = x
        self.y = y if y is not None else x

    def __contains__(self, i):
        if isinstance(i, Interval):
            if i.x not in self:
                return False
            if i.y:
                return i.y in self
        if self.y is None:
            return self.x == i
        else:
            return self.x <= i <= self.y

    def __eq__(self, o):
        if isinstance(o, Interval):
            return o.x == self.x and o.y == self.y

    def Overlap(self, o):
        if self.x > o.y:
            return None
        if o.x > self.y:
            return None
        x = max(o.x, self.x)
        y = min(x.y for x in [self, o] if x.y is not None)
        if x <= y:
            return Interval(x, y)

    def __add__(self, o):
        x, y = o, o
        if isinstance(o, Interval):
            x = o.x
            y = o.y
        if self.y is not None:
            return Interval(self.x + x, self.y + y)
        return Interval(self.x + x)

    def __mul__(self, o):
        if self.y is not None:
            return Interval(self.x * o, self.y * o)
        return Interval(self.x * o)

    def __repr__(self):
        if self.y == self.x:
            return str(self.x)
        else:
            return '[%s : %s]' % (self.x, self.y)
    def __iter__(self):
        if self.y:
            return iter([self.x, self.y])
        else:
            return iter([self.x, self.x])

    def __round__(self):
        return Interval(round(self.x), round(self.y))

eq = lambda x: Interval(x)
bt = lambda x, y: Interval(x, y)
f = fractions.Fraction

inf = float('infinity')
InfMb = MB(eq(inf), eq(inf), eq(inf), eq(inf), eq(inf), eq(inf))

lvls = sorted(list(range(31)) + [f(1, 8), f(1, 4), f(1, 2)])

MONSTER_STATS = {l : MB(None, None, None, None, None, None) for l in lvls}
# prof
for l in lvls:
    if 0 == l:
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = bt(0, 3))
    if l in bt(.01, 9):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(3))
    if l in bt(9 , 12):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(4))
    if l in bt(13 , 16):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(5))
    if l in bt(17 , 20):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(6))
    if l in bt(21 , 24):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(7))
    if l in bt(25 , 28):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(8))
    if l in bt(29 , 31):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(prof = eq(9))

# AC
for l in lvls:
    if l in eq(0):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = bt(0, 13))
    if l in bt(.01, 3):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(13))
    if l in eq(4):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(14))
    if l in bt(5, 7):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(15))
    if l in bt(8, 9):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(16))
    if l in bt(10, 12):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(17))
    if l in bt(13, 16):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(18))
    if l in bt(17, 30):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(ac = eq(19))

# DC
for l in lvls:
    if l in eq(0):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = bt(0, 13))
    if l in bt(.01, 3):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(13))
    if l in eq(4):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(14))
    if l in bt(5, 7):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(15))
    if l in bt(8, 10):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(16))
    if l in bt(11, 12):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(17))
    if l in bt(13,16):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(18))
    if l in bt(17, 20):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(19))
    if l in bt(21, 23):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(20))
    if l in bt(24, 26):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(21))
    if l in bt(27, 29):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(22))
    if l in eq(30):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(save_dc = eq(23))
# attk
for l in lvls:
    if l in eq(0):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = bt(0, 3))
    if l in bt(.01, 2):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(3))
    if l in eq(3):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(4))
    if l in eq(4):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(5))
    if l in bt(5, 7):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(6))
    if l in bt(8, 10):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(7))
    if l in bt(11, 15):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(8))
    if l in eq(16):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(9))
    if l in bt(17, 20):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(10))
    if l in bt(21, 23):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(11))
    if l in bt(24, 26):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(12))
    if l in bt(27, 29):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(13))
    if l in eq(30):
        MONSTER_STATS[l] = MONSTER_STATS[l]._replace(attk = eq(14))

hp_breaks = [1, 7, 36, 50, 71, 86, 101, 116, 131, 146, 161, 176, 191, 206, 221,
        236, 251, 266, 281, 296, 311, 326, 341, 356, 401, 446, 491, 536, 581,
        626, 671, 716, 761, 806, 851]
hps = [bt(x, y-1) for x, y in zip(hp_breaks, hp_breaks[1:])]

dmg_breaks = [1, 2, 4, 6, 9, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75, 81,
        87, 93, 99, 105, 111, 117, 123, 141, 159, 177, 195, 213, 231, 249, 267,
        285, 303, 321]
dmgs = [bt(x, y-1) for x, y in zip(dmg_breaks, dmg_breaks[1:])]

for l, hp, dmg in zip(lvls, hps, dmgs):
    MONSTER_STATS[l] = MONSTER_STATS[l]._replace(hp = hp, dmg=dmg)

Effect = collections.namedtuple('Effect', ['name', 'apply', 'unapply'])

no_effect = lambda x: x

class Plus:
    def __init__(self, p, x):
        self.p = p
        self.x = x
    def __call__(self, mp):
        return mp._replace(**{self.p : getattr(mp, self.p) + self.x})
    def __repr__(self):
        return "<%s + %s>" % (self.p, self.x) 

class Mult:
    def __init__(self, p, x):
        self.p = p
        self.x = x
    def __call__(self, mp):
        return mp._replace(**{self.p : getattr(mp, self.p) * self.x})
    def __repr__(self):
        return "<%s * %.3f>" % (self.p, self.x) 

additive_effect = lambda name, prop, x: Effect(name, Plus(prop, x), Plus(prop, -x))
multiplicative_effect = lambda name, prop, x: Effect(name, Mult(prop, x), Mult(prop, 1/x))

EFFECTS = dict(map(lambda x: (x.name.lower(), x), [
        additive_effect('aggressive', 'dmg', 2),
        additive_effect('ambusher', 'attk', 1),
        additive_effect('avoidance', 'ac', 1),
        additive_effect('blood frenzy', 'attk', 4),
        additive_effect('constrict', 'ac', 1),
        additive_effect('legendary resistance', 'hp', 20),
        additive_effect('magic resistance', 'ac', 2),
        additive_effect('pack tactics', 'attk', 1),
        additive_effect('possession', 'ac', 1),
        additive_effect('rampage', 'dmg', 2),
        additive_effect('regeneration 1/hp', 'hp', 3),
        additive_effect('regeneration 5/hp', 'hp', 15),
        additive_effect('relentless', 'hp', 14),
        additive_effect('shadow stealth', 'ac', 4),
        additive_effect('stench', 'ac', 1),
        additive_effect('superior invisibility', 'ac', 1),
        additive_effect('web', 'ac', 1),
        multiplicative_effect('frightful presence', 'hp', fractions.Fraction(5, 4)),
        Effect('nimble escape',
                lambda mp: mp._replace(ac = mp.ac + 4, attk = mp.attk + 4),
                lambda mp: mp._replace(ac = mp.ac + (-4), attk = mp.attk + (-4))),
] + [
        Effect('amorphous', no_effect, no_effect),
        Effect('amphibious', no_effect, no_effect),
        Effect('angelic weapon', no_effect, no_effect),
        Effect('antimagic susceptibility', no_effect, no_effect),
        Effect('blind senses', no_effect, no_effect),
        Effect('breath weapon', no_effect, no_effect),
        Effect('chameleon skin', no_effect, no_effect),
        Effect('change shape', no_effect, no_effect),
        Effect('charge', no_effect, no_effect),
        Effect('charm', no_effect, no_effect),
        Effect('damage absorption', no_effect, no_effect),
        Effect('death burst', no_effect, no_effect),
        Effect('devil sight', no_effect, no_effect),
        Effect('dive', no_effect, no_effect),
        Effect('echolocation', no_effect, no_effect),
        Effect('elemental body', no_effect, no_effect),
        Effect('enlarge', no_effect, no_effect),
        Effect('etherealness', no_effect, no_effect),
        Effect('false appearance', no_effect, no_effect),
        Effect('fey ancestry', no_effect, no_effect),
        Effect('flyby', no_effect, no_effect),
        Effect('grappler', no_effect, no_effect),
        Effect('hold breath', no_effect, no_effect),
        Effect('illumination', no_effect, no_effect),
        Effect('illusory appearance', no_effect, no_effect),
        Effect('immutable form', no_effect, no_effect),
        Effect('incorporeal movement', no_effect, no_effect),
        Effect('inscrutable', no_effect, no_effect),
        Effect('invisibility', no_effect, no_effect),
        Effect('keen senses', no_effect, no_effect),
        Effect('labyrinthine recall', no_effect, no_effect),
        Effect('leadership', no_effect, no_effect),
        Effect('life drain', no_effect, no_effect),
        Effect('light sensibility', no_effect, no_effect),
        Effect('magic weapons', no_effect, no_effect),
        Effect('martial advantage', no_effect, no_effect),
        Effect('mimicry', no_effect, no_effect),
        Effect('otherwordly perception', no_effect, no_effect),
        Effect('pounce', no_effect, no_effect),
        Effect('psychic defense', no_effect, no_effect),
        Effect('read thoughts', no_effect, no_effect),
        Effect('reckless', no_effect, no_effect),
        Effect('redirect attack', no_effect, no_effect),
        Effect('reel', no_effect, no_effect),
        Effect('rejuvenation', no_effect, no_effect),
        Effect('siege monster', no_effect, no_effect),
        Effect('slippery', no_effect, no_effect),
        Effect('spider climb', no_effect, no_effect),
        Effect('standing leap', no_effect, no_effect),
        Effect('steadfast', no_effect, no_effect),
        Effect('sunlight sensitivity', no_effect, no_effect),
        Effect('surefooted', no_effect, no_effect),
        Effect('surprise attack', no_effect, no_effect),
        Effect('swallow', no_effect, no_effect),
        Effect('teleport', no_effect, no_effect),
        Effect('terrain camouflage', no_effect, no_effect),
        Effect('tunneler', no_effect, no_effect),
        Effect('turn immunity', no_effect, no_effect),
        Effect('turn resistance', no_effect, no_effect),
        Effect('two heads', no_effect, no_effect),
        Effect('web sense', no_effect, no_effect),
        Effect('web walker', no_effect, no_effect),
]))

MONSTER_XP = dict(zip(lvls, [0, 25, 50, 100, 200, 450, 700, 1100, 1800, 2300, 2900, 3900,
        5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000, 20000, 22000,
        25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000,
        155000]))

def RoundToMonsterXp(xp):
    return min(MONSTER_XP.items(), key=lambda x: abs(x[1]-xp))[0]

PC_XP = dict(zip(['easy', 'medium', 'hard', 'deadly'], zip(*([
        (25, 50, 75, 100),
        (50, 100, 150, 200),
        (75, 150, 225, 400),
        (125, 250, 375, 500),
        (250, 500, 750, 1100),
        (300, 600, 900, 1400),
        (350, 750, 1100, 1700),
        (450, 900, 1400, 2100),
        (550, 1100, 1600, 2400),
        (600, 1200, 1900, 2800),
        (800, 1600, 2400, 3600),
        (1000, 2000, 3000, 4500),
        (1100, 2200, 3400, 5100),
        (1250, 2500, 3800, 5700),
        (1400, 2800, 4300, 6400),
        (1600, 3200, 4800, 7200),
        (2000, 3900, 5900, 8800),
        (2100, 4200, 6300, 9500),
        (2400, 4900, 7300, 10900),
        (2800, 5700, 8500, 12700),
]))))

XpMul = collections.namedtuple('XpMul', ['num', 'mul', 'prob'])
XP_MUL = [
        XpMul(eq(1), 1, 7),
        XpMul(eq(2), 1.5, 5),
        XpMul(bt(3,6), 2, 4),
        XpMul(bt(7,10), 2.5, 1),
        XpMul(bt(11, 14), 3, 1),
        XpMul(bt(15, 30), 4, 1),
]

def PickWithProb(l, rand):
    total = sum(p.prob for p in l)
    assert all(x for x in l), l
    assert all(x.prob is not None for x in l), l
    c = rand.uniform(0, total)
    for p in l:
        if c <= p.prob:
            return p
        c -= p.prob
    assert False, "%f %s" % (c, l)

Scenario = collections.namedtuple('Scenario', ['name', 'prob', 'types'])
SCENARIOS = [
    Scenario('Solo', 15, [eq(1)]),
    Scenario('Duo', 15, [eq(1), eq(1)]),
    Scenario('Boss with Minions', 5, [eq(1), bt(2, 15)]),
    Scenario('Trio',2, [eq(1), eq(1), eq(1)]),
    Scenario('Duo with Minions', 2, [eq(1), eq(1), bt(2, 15)]),
    Scenario('Elites with Minions', 2, [bt(1,4), bt(3, 15)]),
    Scenario('Boss with Elites and Minions', 2, [eq(1), bt(2,4), bt(3, 15)]),
    Scenario('Small', 2, [bt(2,15)]),
]

def PickFromInterval(i, rand):
    assert i.x <= i.y, i
    if i.x == i.y:
        return i.x
    return rand.randint(i.x, i.y)

def PickScenarioInterval(interval, rand):
    assert interval.x <= 15
    possible = []
    for s in SCENARIOS:
        ran = sum(s.types, Interval(0, 0))
        o = ran.Overlap(interval)
        if o is not None:
            possible.append(s)
    assert possible

    def TrySolution(interval):
        abstract_scenario = PickWithProb(possible, rand)
        types = []
        for t in abstract_scenario.types:
            if t.Overlap(interval) is None:
                return None, None
            pick = PickFromInterval(t.Overlap(interval), rand)
            types.append(pick)
            interval = Interval(max(0, interval.x - pick), max(0, interval.y - pick))
        return abstract_scenario, types
    for _ in range(1000):
        abstract_scenario, sol = TrySolution(interval)
        if sol is not None:
            break
    assert sol is not None, interval
    assert abstract_scenario is not None
    return Scenario(abstract_scenario.name, abstract_scenario.prob, sol)


def PickScenario(n_monsters=None, rand=None):
    if rand is None:
        rand = random.Random()
    if n_monsters is None:
        n_monsters = bt(1, 30)
    else:
        n_monsters = eq(n_monsters)
    return PickScenarioInterval(n_monsters, rand)

def ReverseEngineer(mb, effects=None):
    """Gives CR for each attributes.

    Args:
        mb: MB with concrete values or None

    Returns:
        MB with lowest explaining CR for each level.
    """
    effects = effects or []
    for e in effects:
        ef = EFFECTS[e.lower()]
        mb = ef.apply(mb)
    lvls = InfMb
    for p in Props:
        if getattr(mb, p) is None:
            lvls = lvls._replace(**{p : None})
        for l, m in reversed(sorted(list(MONSTER_STATS.items()))):
            if getattr(mb, p) is not None and getattr(mb, p) in getattr(m, p):
                lvls = lvls._replace(**{p : l})
    return lvls

def Avg(mb):
    """Gives average CR for attributes.

    Args:
        mb: MB with concrete values or None

    Returns:
        float for average
    """
    v = []
    for p in Props:
        if getattr(mb, p) is not None:
            v.append(getattr(mb, p))
    return sum(v)/len(v)

def DistributeWithAvg(avg, n, rand, sig=2, max=30):
    for i in range(1000):
        deviations = [rand.gauss(avg, sig) for i in range(n-1)]
        vals = [int(avg) for x in deviations]
        vals.append(avg * n - sum(vals))
        valsi = list(map(int, vals))
        if all(0 <= i <= max for i in valsi):
            return valsi
    return [avg] * n


Attk = collections.namedtuple('Attk', [
        'dmg',
        'area',
        'save_based',
        'effects',
        'multi_attk',
])

def MakeAttacks(mb, rand):
    attks = []
    AttkType = collections.namedtuple('AttkType', [
            'prob',
            'area',
            'save_based',
            'effects',
            'multi_attk',
    ])
    attk_types = [
            AttkType(10, False, False, [], True),
            AttkType(3, True, True, [], False),
            AttkType(1, True, False, [], False),
    ]
    Area = collections.namedtuple('Area', [
            'prob',
            'shape',
            'range'
    ])
    areas = [
            Area(10, 'cube', '15ft'),
            Area(5, 'line', '30ft'),
            Area(5, 'cone', '15ft'),
            Area(1, 'cone', '60ft'),
    ]
    taken = []
    num = sum(rand.random() > 0.5 for _ in range(3))+1
    for i in range(num):
        for i in range(10):
            attk_type = PickWithProb(attk_types, rand)
            if attk_type not in taken:
                taken.append(attk_type)
                break
        dmg = PickFromInterval(mb.dmg, rand)
        assert dmg >0
        n_attks = 1
        dmg_per_attk = round(dmg / n_attks)
        area = None
        assert dmg_per_attk > 0
        if attk_type.area:
            dmg_per_attk = dmg // 2
            area = PickWithProb(areas, rand)
        if attk_type.multi_attk:
            n_attks = 1 + dmg_per_attk // 20
            assert n_attks > 0
            dmg_per_attk = math.ceil(dmg_per_attk / n_attks)
        effects = attk_type.effects
        attk = Attk(dmg_per_attk, area, attk_type.save_based,
                effects, n_attks)
        if attk not in attks:
            attks.append(attk)
    assert attks
    return sorted(attks, key=lambda x: (x.multi_attk, x.dmg, x.area is None))

def RandomMonster(cr, rand=None, effects=None):
    assert cr in lvls
    effects = effects or []
    if rand is None:
        rand = random.Random()
    defensive, offensive = DistributeWithAvg(cr, 2, rand)

    ac, hp = DistributeWithAvg(defensive, 2, rand)

    attk, dmg, dc = DistributeWithAvg(offensive, 3, rand)

    assert ac in lvls, ac
    assert hp in lvls, hp
    assert attk in lvls, attk
    assert dmg in lvls,dmg
    assert dc in lvls, dc

    mb = MB(
            MONSTER_STATS[cr].prof,
            MONSTER_STATS[ac].ac,
            MONSTER_STATS[hp].hp,
            MONSTER_STATS[attk].attk,
            MONSTER_STATS[dmg].dmg,
            MONSTER_STATS[dc].save_dc,
    )
    def Valid(m):
        if m.prof.y < 0: return False
        if m.ac.y <= 0: return False
        if m.hp.y <= 0: return False
        if m.dmg.y <= 0: return False
        if m.save_dc.y <= 0: return False
        return True

    def Effects():
        m = mb
        n_effects = sum([rand.random() < 0.25 for i in range(7)])
        es = rand.sample(list(EFFECTS.keys()), n_effects)
        es = [EFFECTS[e] for e in (effects + es)]
        for e in es:
            m = e.unapply(m)
            if not Valid(m):
                return None
        return m, es
    for i in range(1000):
        e = Effects()
        if e is not None:
            mb, effects = e
            break

    def Clip(i):
        return Interval(int(max(i.x, 0)), int(max(i.y, 0)))
    for p in Props:
        if p == 'dmg': continue
        mb = mb._replace(**{p : PickFromInterval(Clip(getattr(mb, p)), rand)})
    ProbOfAttkAttr = collections.namedtuple('ProbOfAttkAttr', ['attr', 'prob'])
    attk_ability = PickWithProb([
        ProbOfAttkAttr('str', 10),
        ProbOfAttkAttr('dex', 5),
        ProbOfAttkAttr('int', 1),
        ProbOfAttkAttr('wis', 1),
        ProbOfAttkAttr('cha', 1),
    ], rand).attr
    attk_attr_mod = mb.attk - mb.prof
    attr = dict(
            str=None,
            dex=None,
            con=None,
            int=None,
            wis=None,
            cha=None)
    attr[attk_ability] = attk_attr_mod
    if attk_ability != 'dex':
        attr['dex'] = mb.ac - 10
    for key in attr:
        if attr[key] is None:
            attr[key] = int(rand.gauss(cr/3, 2))
    attks = MakeAttacks(mb, rand)
    return Monster(
            mb,
            [e.name for e in effects],
            cr,
            attk_ability,
            attr,
            attks)

class AttrCombo:
    def __init__(self, effect, *attrs):
        self.effect = effect
        self.attrs = attrs

    def __repr__(self):
        return "%s(%r, %r)" % (self.__class__.__name__, self.effect, self.attrs)

    def __call__(self, m):
        if m.attk_attr in self.attrs and self.effect in m.effects:
            return 1
        else:
            return 0

class EffectCombo:
    def __init__(self, *args):
        self.effects = args

    def __call__(self, m):
        n_match = sum(1 for x in self.effects if x in m.effects) - 1
        return n_match / (len(self.effects) - 1)

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, ', '.join(self.effects))


good_effect_combos = [
        # Attribute based combos
        AttrCombo('aggressive','str'),
        AttrCombo('ambusher','dex'),
        AttrCombo('avoidance','dex'),
        AttrCombo('blood frenzy','dex'),
        AttrCombo('magic resistance','int','wis','cha'),
        AttrCombo('possession','int','wis','cha'),
        AttrCombo('rampage','str'),
        AttrCombo('shadow stealth','dex'),
        AttrCombo('nimble escape','dex'),
        AttrCombo('chameleon skin','dex'),
        AttrCombo('charge','str'),
        AttrCombo('charm','cha'),
        AttrCombo('enlarge','str'),
        AttrCombo('grappler','str'),
        AttrCombo('keen senses','dex'),
        AttrCombo('leadership','cha'),
        AttrCombo('pounce','str'),
        AttrCombo('psychic defense','wis'),
        AttrCombo('read thoughts','wis', 'cha'),
        AttrCombo('reckless','str'),
        AttrCombo('siege monster','str'),
        AttrCombo('slippery','dex'),
        AttrCombo('surprise attack','dex'),
        EffectCombo('superior invisibility', 'ambusher'),
        EffectCombo('blood frenzy', 'aggressive', 'pack tactics', 'relentless'),
        EffectCombo('web', 'web sense', 'web walker'),
        EffectCombo('turn immunity', 'life drain'),
]

def ScoreMonster(monster):
    num_area = sum(1 for m in monster.attks if m.area is not None)
    n_better_than_main = sum(1 for m in monster.attr.values() if m >
            monster.attr[monster.attk_attr])
    n_effects = len(monster.effects)
    combos = sum(1 for x in good_effect_combos if x(monster))
    attk_groups = [list(i) for _, i in 
            itertools.groupby(monster.attks,
                    lambda a: (a.area, a.save_based, a.effects, a.multi_attk))]
    attk_grp_score = sum(map(len, attk_groups))
    assert attk_grp_score >= 0
    return (combos
            + (n_effects - 2.)**2
            - num_area
            - n_better_than_main
            - attk_grp_score**2)

def MakeScoredMonster(n, *args, **kwargs):
    return max([RandomMonster(*args, **kwargs) for _ in range(n)],
            key=ScoreMonster)

def Encounter(character_lvls, difficulty='medium', n_monsters=None, rand=None):
    assert difficulty in ['easy', 'medium', 'hard', 'deadly']
    if rand is None:
        rand = random.Random()
    xps = 0
    max_lvl = min(character_lvls) + 3
    for c in character_lvls:
        xps += PC_XP[difficulty][c-1]
    xps = int(xps*1.15)
    scenario = PickScenario(n_monsters, rand)
    xp_budget = DistributeWithAvg(xps/len(scenario.types), len(scenario.types),
            rand, sig=xps/5, max=float('infinity'))
    cr_budget = [RoundToMonsterXp(x/t) for x, t in zip(xp_budget, scenario.types)]
    monsters = [MakeScoredMonster(10, cr, rand) for cr in cr_budget]

    return list(zip(scenario.types, monsters))

def ScoreEncounter(enc):
    n = sum(i for i, _ in enc)
    n_scores = [
            (bt(1, 5), 1),
            (bt(6, 10), 3),
            (bt(11, 20), 7),
            (bt(21, 40), 10)
    ]
    n_score = [s for i, s in n_scores if n in i][0]
    overlapping_effects = collections.Counter()
    for _, m in enc:
        overlapping_effects.update(m.effects)
    areas_of_multi_monster = sum(1 for i, m in enc if i > 1 and any(a.area is
            not None for a in m.attks))
    leadership_of_solo = sum(1 for i, m in enc if i == 1 and 'leadership' in
            m.effects and any(j > 1 for j, _ in enc))
    return (- n_score
            + sum(overlapping_effects.values())
            - len(overlapping_effects.keys())
            - areas_of_multi_monster
            + leadership_of_solo
            )

def ScoredEncounter(n, *args, **kwargs):
    return max((Encounter(*args, **kwargs) for _ in range(n)), key=ScoreEncounter)
