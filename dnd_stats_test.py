import dnd_stats

import pstats
from io import StringIO
import copy
import unittest
import hypothesis
import cProfile

from hypothesis import strategies

import collections
import math
import fractions

PList = collections.namedtuple('PList', ['prob'])

class TestPickWithProb(unittest.TestCase):
    @hypothesis.given(strategies.lists(strategies.integers(min_value=0)),
            strategies.randoms())
    def test_PickWithProb(self, l, r):
        hypothesis.assume(l)
        hypothesis.assume(any(p > 0 for p in l))
        probs = list(map(PList, l))
        p = dnd_stats.PickWithProb(probs, r)
        self.assertIn(p, probs)
        self.assertNotEqual(p.prob, 0)

def AssumeSaneNumbers(*args):
    hypothesis.assume(not any(math.isnan(x) for x in args))
    hypothesis.assume(not any(math.isinf(x) for x in args))

class TestInterval(unittest.TestCase):
    @hypothesis.given(strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100))
    def test_Midpoint(self, l, r):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval = dnd_stats.Interval(l, r)
        self.assertIn(l+(r-l)/2, interval)
        self.assertIn(l, interval)
        self.assertIn(r, interval)

    @hypothesis.given(strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100))
    def test_Add(self, l, r, add):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r, add)
        interval = dnd_stats.Interval(l, r)
        interval_mod = interval + add
        self.assertIn(l+(r-l)/2+add, interval_mod)

    @hypothesis.given(strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100))
    def test_Mul(self, l, r, mul):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r, mul)
        interval = dnd_stats.Interval(l, r)
        interval_mod = interval * mul
        self.assertIn((l+(r-l)/2) * mul, interval_mod)
        self.assertIn(l* mul, interval_mod)
        self.assertIn(r* mul, interval_mod)

    @hypothesis.given(strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100))
    def test_Iter(self, l, r):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval = dnd_stats.Interval(l, r)
        self.assertIn(l, list(interval))
        self.assertIn(r, list(interval))

    @hypothesis.given(strategies.floats(min_value=0, max_value=100),
            strategies.floats(min_value=0, max_value=100))
    def test_Repr(self, l, r):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval = dnd_stats.Interval(l, r)
        self.assertIn(repr(l), repr(interval))
        self.assertIn(repr(r), repr(interval))


class OverlapTest(unittest.TestCase):
    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100))
    def test_OverlapMidpoint(self, l, r):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval_a = dnd_stats.Interval(l, r)
        interval_b = dnd_stats.Interval((l+r)/2, (l+r)/2)
        self.assertNotEqual(None, interval_a.Overlap(interval_b))

    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            )
    def test_OverlapLeft(self, l, r, sub):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval_a = dnd_stats.Interval(l, r)
        self.assertEqual(dnd_stats.Interval(l, l),
                interval_a.Overlap(dnd_stats.Interval(l, l)))
        self.assertEqual(dnd_stats.Interval(l, l),
                interval_a.Overlap(dnd_stats.Interval(l-sub, l)))

    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100))
    def test_OverlapRight(self, l, r, add):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval_a = dnd_stats.Interval(l, r)
        self.assertEqual(dnd_stats.Interval(r, r),
                interval_a.Overlap(dnd_stats.Interval(r, r)))
        self.assertEqual(dnd_stats.Interval(r, r),
                interval_a.Overlap(dnd_stats.Interval(r, r+add)))

    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100))
    def test_OverlapRight(self, l, r):
        hypothesis.assume(l <= r)
        AssumeSaneNumbers(l, r)
        interval_a = dnd_stats.Interval(l, r)
        self.assertEqual(dnd_stats.Interval(l, (l+r)/2),
                interval_a.Overlap(dnd_stats.Interval(l, (l+r)/2)))
        self.assertEqual(dnd_stats.Interval((l+r)/2, r),
                interval_a.Overlap(dnd_stats.Interval((l+r)/2, r)))

    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100)
            )
    def test_OverlapSmart(self, l, r, ll, rr):
        AssumeSaneNumbers(l, r, ll, rr)
        hypothesis.assume(l <= r)
        hypothesis.assume(ll <= rr)
        hypothesis.assume(r < rr)
        hypothesis.assume(ll < r)
        hypothesis.assume(l < ll)
        interval_a = dnd_stats.Interval(l, r)
        interval_b = dnd_stats.Interval(ll, rr)
        self.assertEqual(dnd_stats.Interval(ll, r),
                interval_a.Overlap(interval_b))

    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100)
            )
    def test_OverlapLeftOf(self, l, r, ll, rr):
        AssumeSaneNumbers(l, r, ll, rr)
        hypothesis.assume(l <= r)
        hypothesis.assume(ll <= rr)
        hypothesis.assume(r < ll)
        interval_a = dnd_stats.Interval(l, r)
        interval_b = dnd_stats.Interval(ll, rr)
        self.assertEqual(None,
                interval_a.Overlap(interval_b))

    @hypothesis.given(
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100),
            strategies.integers(min_value=0, max_value=100)
            )
    def test_OverlapCommutative(self, l, r, ll, rr):
        AssumeSaneNumbers(l, r, ll, rr)
        hypothesis.assume(l <= r)
        hypothesis.assume(ll <= rr)
        interval_a = dnd_stats.Interval(l, r)
        interval_b = dnd_stats.Interval(ll, rr)
        interval_c = dnd_stats.Interval(ll)
        interval_d = dnd_stats.Interval(l)
        self.assertEqual(interval_b.Overlap(interval_a),
                interval_a.Overlap(interval_b))

        self.assertEqual(interval_c.Overlap(interval_d),
                interval_d.Overlap(interval_c))

        self.assertEqual(interval_c.Overlap(interval_b),
                interval_b.Overlap(interval_c))

        self.assertEqual(interval_d.Overlap(interval_a),
                interval_a.Overlap(interval_d))

class PickScenarioTest(unittest.TestCase):
    @hypothesis.given(strategies.integers(min_value=1, max_value=15),
            strategies.integers(min_value=1, max_value=15), strategies.randoms())
    def test_HasSolution(self, l, r, rand):
        hypothesis.assume(l <= r)
        possible = dnd_stats.PickScenarioInterval(dnd_stats.Interval(l, r), rand)
        self.assertNotEqual([], possible)

    @hypothesis.given(strategies.integers(min_value=1, max_value=15), strategies.randoms())
    def test_HasSolutionSimple(self, l, rand):
        possible = dnd_stats.PickScenario(l, rand)
        hypothesis.note(possible)
        self.assertNotEqual(None, possible)
        hypothesis.note(possible)
        for p in possible.types:
            self.assertIsInstance(p, int)
        self.assertEqual(l, sum(possible.types))

    @hypothesis.given(strategies.randoms())
    def test_HasSolutionSimpleNoNumber(self, rand):
        possible = dnd_stats.PickScenario(rand=rand)
        hypothesis.note(possible)
        self.assertNotEqual(None, possible)
        hypothesis.note(possible)
        for p in possible.types:
            self.assertIsInstance(p, int)


class RandomMonster(unittest.TestCase):
    @hypothesis.given(strategies.integers(min_value=1, max_value=30), strategies.randoms())
    def test_Average(self, cr, r):
        monster = dnd_stats.RandomMonster(cr,r)
        hypothesis.note(monster)
        for e in monster.effects:
            self.assertIsInstance(e, str)
        for p in dnd_stats.Props:
            if p == 'dmg': continue
            self.assertIsInstance(getattr(monster.template, p), int)
        rev = dnd_stats.ReverseEngineer(monster.template, effects=monster.effects)
        for e in rev:
            self.assertIsInstance(e, (int, fractions.Fraction))
        hypothesis.note(rev)
        self.assertLessEqual(dnd_stats.Avg(rev), cr)

    @hypothesis.given(strategies.integers(min_value=1, max_value=30),
            strategies.integers(min_value=1, max_value=30), strategies.randoms())
    def test_Distribute(self, cr, n, r):
        res = dnd_stats.DistributeWithAvg(cr, n, r)
        hypothesis.note(res)
        self.assertEqual(n, len(res))
        self.assertEqual(cr * n, sum(res))
        for r in res:
            self.assertIsInstance(r, int)
            self.assertLessEqual(0, r)
            self.assertLessEqual(r, 30)

class EncounterBuilding(unittest.TestCase):
    @hypothesis.given(strategies.lists(strategies.integers(min_value=1, max_value=20)),
            strategies.integers(min_value=1, max_value=10),
            strategies.randoms(), strategies.sampled_from(['easy', 'medium', 'hard', 'deadly']))
    def test_Encounter(self, levels, n, r, d):
        hypothesis.assume(levels)
        enc = dnd_stats.Encounter(levels, difficulty=d, n_monsters=n, rand=r)
        hypothesis.note(enc)
        self.assertIsInstance(enc, list)
        for l in enc:
            self.assertIsInstance(l, tuple)
            num, monster = l
            self.assertIsInstance(num, int)
            self.assertGreaterEqual(num, 1)
            self.assertIsInstance(monster, dnd_stats.Monster)

    @hypothesis.given(strategies.integers(min_value=0))
    def test_RoundToMonster(self, xp):
        self.assertIn(dnd_stats.RoundToMonsterXp(xp), dnd_stats.lvls)

Mbs =  strategies.builds(dnd_stats.MB,
                    strategies.integers(min_value=3, max_value=10),
                    strategies.integers(min_value=10, max_value=20),
                    strategies.integers(min_value=1, max_value=1000),
                    strategies.integers(min_value=1, max_value=20),
                    strategies.integers(min_value=1, max_value=150),
                    strategies.integers(min_value=10, max_value=30))

class EffectsTest(unittest.TestCase):
    @hypothesis.given(
            strategies.sampled_from(dnd_stats.EFFECTS.values()),
            Mbs)
    # With rounding this will give a rounding error
    @hypothesis.example(dnd_stats.EFFECTS['frightful presence'],
            dnd_stats.MB(prof=3, ac=10, hp=257, attk=1, dmg=1, save_dc=10))
    def test_Inverse(self, effect, prop):
        self.assertEqual(prop, effect.apply(effect.unapply(prop)))

class ScoreMonsterTest(unittest.TestCase):

    RandomMonsters = strategies.builds(dnd_stats.RandomMonster,
            strategies.integers(min_value=1, max_value=30), strategies.randoms())
    ScoredMonsters = strategies.builds(dnd_stats.MakeScoredMonster,
strategies.integers(min_value=1, max_value=30),
            strategies.integers(min_value=1, max_value=30),
            strategies.randoms())
    @hypothesis.given(RandomMonsters)
    def test_IsNumber(self, monster):
        self.assertIsInstance(dnd_stats.ScoreMonster(monster), float)

    @hypothesis.given(RandomMonsters,
            strategies.sampled_from(dnd_stats.good_effect_combos))
    def test_EffectCombo(self, monster, combo):
        self.assertLessEqual(combo(monster), 1)

    @hypothesis.given(ScoredMonsters)
    def test_ScoredIsNumber(self, monster):
        self.assertIsInstance(dnd_stats.ScoreMonster(monster), float)

    @hypothesis.given(ScoredMonsters,
            strategies.integers(min_value=1, max_value=5))
    def test_CopiedAttksAreBad(self, monster, copied):
        hypothesis.assume(copied < len(monster.attks))
        hypothesis.note(monster)
        monster2 = copy.deepcopy(monster)
        monster2.attks.append(monster2.attks[copied])
        hypothesis.note(monster2)
        self.assertLess(dnd_stats.ScoreMonster(monster2), dnd_stats.ScoreMonster(monster))

        monster3 = copy.deepcopy(monster)
        monster3.attks.append(monster2.attks[copied]._replace(dmg=1))
        hypothesis.note(monster3)
        self.assertLess(dnd_stats.ScoreMonster(monster3), dnd_stats.ScoreMonster(monster))


class ScoreEncounterTest(unittest.TestCase):
    Enc = strategies.builds(dnd_stats.Encounter,
            strategies.lists(
                    strategies.integers(min_value=1, max_value=20), min_size=1,
                    max_size=10),
            strategies.sampled_from(['easy', 'medium', 'hard', 'deadly']), 
            rand=strategies.randoms())

    @hypothesis.given(Enc)
    def test_IsNumber(self, enc):
        self.assertIsInstance(dnd_stats.ScoreEncounter(enc), float)

    @hypothesis.given(Enc, strategies.integers(min_value=1, max_value=5))
    def test_MoreMonstersIsWorse(self, enc, i):
        hypothesis.assume(i < len(enc))
        enc2 = copy.deepcopy(enc)
        enc2[i] = enc2[i][0] + 1, enc2[i][1]
        self.assertLess(dnd_stats.ScoreEncounter(enc2), dnd_stats.ScoreEncounter(enc))

    @hypothesis.given(Enc, strategies.integers(min_value=1, max_value=5), strategies.integers(min_value=1, max_value=5))
    def test_CombosAreGood(self, enc, i, j):
        hypothesis.assume(i < len(enc))
        hypothesis.assume(j < len(enc))
        hypothesis.assume(i != j)
        hypothesis.assume(enc[i][1].effects)
        hypothesis.assume(enc[i][1].effects != enc[j][1].effects)
        hypothesis.note(enc[i][1].effects)
        hypothesis.note(enc[j][1].effects)
        enc2 = copy.deepcopy(enc)
        enc2[j][1].effects.extend(enc[i][1].effects)
        hypothesis.note(enc2)

        self.assertLess(dnd_stats.ScoreEncounter(enc),
                dnd_stats.ScoreEncounter(enc2))


    def test_Profile(self):
        return
        pr = cProfile.Profile()
        pr.enable()
        dnd_stats.Encounter([6]*5)
        pr.disable()

        s = StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())


if __name__ == '__main__':
    unittest.main()
